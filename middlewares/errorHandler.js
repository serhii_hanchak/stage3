module.exports = function errorHandler (err, req, res, next) {
  const status = err.status || 500
  const responseData = {
      error : true,
      message : err.message || err,
  }
  console.error(err.message || err);
  res.status(status).send(responseData)
}
