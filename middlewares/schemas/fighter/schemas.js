const Joi = require('@hapi/joi');

const fighterSchema = Joi.object().keys({
  name: Joi.string().required(),
  health: Joi.number().positive().min(1).required(),
  power: Joi.number().min(0).max(100).required(),
  defense: Joi.number().min(1).max(10).required()
})

module.exports = fighterSchema
