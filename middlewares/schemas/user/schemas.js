const Joi = require('@hapi/joi');
const gmailPattern = /^[\w.+\-]+@gmail\.com$/
const phonePattern = /^\+?3?8?(0\d{9})$/

const userSchema = Joi.object().keys({
  firstName: Joi.string().required(),
  lastName: Joi.string().min(3).max(20).required(),
  email: Joi.string().regex(gmailPattern).required(),
  phoneNumber: Joi.string().regex(phonePattern).required(),
  password: Joi.string().min(3).required()
})

module.exports = userSchema

