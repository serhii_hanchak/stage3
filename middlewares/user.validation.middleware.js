const { user } = require('../models/user');
const Joi = require("@hapi/joi");
const createError = require("http-errors");

module.exports = async function validation (object, schema) {
  try {
    await Joi.validate(object, schema, { abortEarly: false });
    return object;
  } catch (err) {
    throw new createError(
      400,
      { error: true, message: `${err.message}` }
    );
  }
};
