const { Router } = require('express');
const FighterService = require('../services/fighterService')
const finghterSchema = require('../middlewares/schemas/fighter/schemas')
const validate = require('../middlewares/user.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    const fighters = FighterService.getAllFighters();
    if (fighters.length != 0) {
      return res.json(fighters);
    } else {
      res.status(404).send({
        error: true, message: "The database is empty"
      });
    }
  } catch (err) {
    return next(err);
  }
});
router.get('/:id', (req, res, next) => {
  try {
    const { id } = req.params;
    const user = FighterService.getOneFighter({ id });
    if (user) {
      return res.json(user);
    } else {
      return res
        .status(404)
        .send({ error: true, message: `There is no fighter with id: ${id}` });
    }
  }
  catch (err) {
    return next(err);
  }
});
router.post('/', async (req, res, next) => {
  try {
    const { name, health, power, defense } = req.body;
    let fighter;
    const ifNameExist = FighterService.getOneFighter({ name });

    if (!ifNameExist) {
      const data = await validate({ health, power, defense, name }, finghterSchema);
      fighter = FighterService.createFighter(data);
    }
    return fighter ? res.send(fighter) : res.status(400).send({ error: true, message: 'Name already used' })
  } catch (error) {
    return next(error);
  }
});
router.put('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const {
      name,
      health,
      power,
      defense
    } = req.body

    const matchFighter = FighterService.getOneFighter({ id });
    if (matchFighter) {
      const data = await validate({
        name,
        health,
        power,
        defense
      }, finghterSchema)

      const user = FighterService.updateFighter(id, data)
      res.send(user)
    } else {
      return res
        .status(404)
        .send({ error: true, message: `There is no fighter with id: ${id}` });
    }
  } catch (err) {
    return next(err);
  }
})
router.delete('/:id', (req, res, next) => {
  try {
    const { id } = req.params;

    const matchFighter = FighterService.getOneFighter({ id });

    if (matchFighter) {
      FighterService.removeFighter(id);
      res.send({ message: 'Fighter sucessfully removed' });
    } else {
      return res
        .status(404)
        .send({ error: true, message: `There is no fighter with id: ${id}` });
    }
    res.send({ message: 'Sucessfully removed' })
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
