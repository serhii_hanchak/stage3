const { Router } = require('express');
const UserService = require('../services/userService');
const userSchema = require('../middlewares/schemas/user/schemas')
const validate = require('../middlewares/user.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    const users = UserService.getAllUsers();
    if (users.length != 0) {
      return res.json(users);
    } else {
      res.status(404).send({
        error: true, message: "The database is empty"
      });
    }
  } catch (err) {
    return next(err);
  }
});
router.get('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = UserService.getOneUser({ id });
    if (user) {
      return res.json(user);
    } else {
      return res
        .status(404)
        .send({ error: true, message: `There is no user with id: ${id}` });
    }
  }
  catch (err) {
    return next(err);
  }
});
router.post('/', async (req, res, next) => {
  try {
    const { email, ...params } = req.body
    let user

    const data = await validate({ ...params, email }, userSchema);
    const emailExist = UserService.getOneUser({ email });
    if (!emailExist) {
      user = UserService.createUser(data);
    }
    return user ? res.send(user) : res.status(400).send({ error: true, message: 'Email already used' })
  } catch (error) {
    return next(error);
  }
});
router.put('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      password
    } = req.body;

    const matchUser = UserService.getOneUser({ id });

    if (matchUser) {
      const data = await validate({
        firstName,
        lastName,
        phoneNumber,
        email,
        password
      }, userSchema)
      const user = UserService.updateUser(id, data)
      res.send(user)
    } else {
      return res
        .status(404)
        .send({ error: true, message: `There is no user with id: ${id}` });
    }
  } catch (err) {
    return next(err);
  }
})
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = UserService.getOneUser({ id });

    if (user) {
      UserService.removeUser(id);
      res.send({ message: 'User sucessfully removed' });
    } else {
      return res
        .status(404)
        .send({ error: true, message: `There is no user with id: ${id}` });
    }
  } catch (err) {
    return next(err);
  }
})

module.exports = router;
