const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAllFighters () {
    const fighters = FighterRepository.getAll()
    return !fighters ? null : fighters
  }

  getOneFighter (search) {
    const fighter = FighterRepository.getOne(search)
    return fighter
  }

  createFighter (data) {
    const fighter = FighterRepository.create(data)
    return fighter
  }

  updateFighter (id, data) {
    const fighter = FighterRepository.update(id, data)
    return fighter
  }

  removeFighter (id) {
    return FighterRepository.delete(id)
  }
}

module.exports = new FighterService();
