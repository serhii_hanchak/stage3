const { UserRepository } = require('../repositories/userRepository');

class UserService {

  // TODO: Implement methods to work with user

  search (search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getAllUsers () {
    const users = UserRepository.getAll()
    return !users ? [] : users
  }

  getOneUser (search) {
    const user = UserRepository.getOne(search)
    return user
  }

  createUser (data) {
    const user = UserRepository.create(data)
    return user
  }

  updateUser (id, data) {
    const user = UserRepository.update(id, data)
    return user
  }

  removeUser (id) {
    return UserRepository.delete(id)
  }
}

module.exports = new UserService();
